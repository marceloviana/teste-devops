## Teste DevOps - Marcelo Viana
#### Tecnologias abordadas: Cloud AWS, TerraForm, Ansible, Docker e Kubernetes.

Este documento irá guiá-lo em uma jornada DevOps, passando pelos principais stacks voltados para infraestrura como código e aplições escaláveis e performáticas.

Vamos começar com um panorama geral do fluxo percorrido e da arquitetura abordada

Fluxo dos trabalhos

![alt text](diagramas/diagrama-fluxo-provisionamento-ambiente-aplicacao.jpg)

<a href="![alt text](diagramas/diagrama-fluxo-provisionamento-ambiente-aplicacao.jpg)">Download</a>


Diagrama da arquitetura

![alt text](diagramas/diagrama-arquitetura-aplicacao-teste-devops.jpg)

<a href="![alt text](diagramas/diagrama-arquitetura-aplicacao-teste-devops.jpg)">Download</a>

### Pré requisitos:
- Conta AWS Console Cloud
- Conhecimento básico AWS CLI
- Familiaridade com linha de comando
- Conhecimento intermediário de Docker, Kubernetes, TerraForm e Ansible.
- Ter instalado TerraForm, Ansible, GIT e AWS CLI
- Ter uma chave SSH vinculado a um usuário na console AWS IAM

### Instalação da AWS CLI
```
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
```
Referência: https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html


### Configuração das credenciais para AWS CLI
Navegue até sua conta da AWS e em serviço IAM, encontre suas credenciais para acesso via CLI.

Teste suas credenciais:
```
aws sts get-caller-identity
```

O retorno do comando deve ser algo parecido com isso:
```
{
    "UserId": "AID**************J4RJ",
    "Account": "*62********5",
    "Arn": "arn:aws:iam::*62********5:user/marceloviana"
}

```
Caso contrário, execute o comando abaixo e responda as perguntas para configurar o AWS CLI em seu host local:
```
aws configure
```
O resultado será algo assim:
```
AWS Access Key ID [****************UNLK]:
AWS Secret Access Key [****************CuL9]:
Default region name [us-west-2]:
Default output format [None]:
```
### Obtendo os scripts IaC (Infrastructure as code)
Baixe o repositório:
```
git clone gitlab.com:marceloviana/teste-devops.git teste-devops-marceloviana
cd teste-devops-marceloviana
```

### Provisionando uma instância EC2 t2.medium na AWS com TerraForm

Dentro do repositório, navegue para o TerraForm:
```
cd terraform
```

Abra o arquivo main.tf e substitua o nome da chave SSH contido na linha 15, para o nome de uma chave válida da sua conta AWS que vocẽ utiliza para acessar instâncias:
```
vim main.tf
# substitua 'aws-ubuntu-ec2' para o nome de sua chave
key_name = "aws-ubuntu-ec2"
```
Nota: Sem a devida chave configurada, o Ansible não poderá trabalhar nos passos seguintes.


Executando o terraform:

```
# Inicializa o terraform no diretório atual e instala bibliotecas necessários conforme configuração do seu Provider
terraform init

# Faz um pré-build das instruções e exibe uma prévia do plano a ser executado
terraform plan

# Executa o plano de provisionamento
terraform apply

```
O processo acima pode demorar um pouco.

O comando *terraform apply* irá executar o arquivo *main.tf* contido na raiz. Este arquivo contém instruções para conectar na AWS utilizando as credenciais contidas em sua home de usuário *~/.aws/credentials* e provisionará uma instância EC2 t2.medium com recursos suficientes para seguir com as demais tarefas que proveram uma aplicação escalável e gerenciável utilizando o Kubernetes.

Detalhes do arquivo main.tf:
```
variable "region" {
    default = "us-east-1"
}
variable "shared_cred_file" {
    default = "~/.aws/credentials"
}
provider "aws" {
    region = "${var.region}"
    shared_credentials_file = "${var.shared_cred_file}"
    profile = "default"
}
resource "aws_instance" "ubuntu" {
    ami = "ami-00a208c7cdba991ea"
    instance_type = "t2.medium"
    key_name = "aws-ubuntu-ec2"
    tags {
        Name = "UbuntuTerraForm"
    }
}
```
###  Conhecendo e executando o Ansible para instalação do Kubernetes e demais aplicativos

O Ansible é uma ferramenta de gerenciamento e implantação de aplicativos para um ou mais servidores remotos. Possui uma característica interessante de conexão, utiliza apenas conexão SSH e não necessita de agente para se conectar.

Utilizaremos o Ansible para instalar os pacotes complementares e o Kubernetes em nosso servidor recém criado pelo TerraForm.

O Ansible possui um arquivo de inventário que devemos cadastrar os servidores que queremos gerenciar.

Com nosso EC2 *UbuntuTerraForm* criado no passo anterior, obtenha o IP público gerado para a instância. Para isso vá na Console da AWS e em EC2 encontre a instância *UbuntuTerraForm*, na coluna *IPV4 Public IP*. Copie o IP e vamos para o passo seguinte.

Agora vamos inserir o IP ao inventário do Ansible

```
sudo vim /etc/ansible/hosts

# Substitua '3.235.176.15' pelo endereço IP da instância UbuntuTerraForm
# Substitua 'aws-ubuntu-ec2.pem' para o nome da sua chave SSH utilizada para conectar a uma instância

[aws]
3.235.176.15 ansible_ssh_private_key_file=~/.ssh/aws-ubuntu-ec2.pem ansible_user=ubuntu

[aws:vars]
ansible_python_interpreter=/usr/bin/python3
```
Três pontos importantes aqui:
- Variável *ansible_ssh_private_key_file*: esta variável indica chave SSH que o Ansible deve usar para conectar à instância.
- Variável *ansible_user*: indica qual usuário está vinculado a chave informada em *ansible_ssh_private_key_file*.
- `[aws:vars]` isso declara *aws* como variável e seu valor passa a ser:
`ansible_python_interpreter=/usr/bin/python3` onde *ansible_python_interpreter* indica a versão do python que o Ansible usará para executar rotinas. Este conjunto é vinculado a um ou mais grupos de servidores no arquivo de inventário.

Navegue para o Ansible:
```
cd ..
cd ansible
ls -ls
```
Veja que o Ansible possui arquivos YML. Estes arquivos chamamos de *playbooks*, que são descritores de implantação.

Nosso Ansible possui um arquivo playbook.yml na raiz e suas dependências, dentro do diretório 'includes/' para manter separação de responsabilidades e mais organizado.

O Ansible irá:
- Instalar o Kubernetes (kubectl e minikube).
- Instalar o Docker para que o minikube possa usar Driver e outros recursos do Docker.
- Copiar os arquivos das aplicações que serão provisionadas na máquina local para a instância remota.
- Iniciar um cluster com minikube.
- Implantar a primeira aplicação de `BACK-END com duas réplicas, gerenciadas pelo controlador do tipo Deployment` que cuida da saúde dos Pods.
- Implantar a segunda aplicação de `FRONT-END com uma réplica, não gerenciada.` Esta aplicação roda em um Docker fora do kubernetes descrito por um docker-composer.
- `Implanta o serviço de Loadbalance para que o FRON-END possa comunicar com os Pods do BACK-END` e balancelar a carga dos containers clusterizados.


Destro do nosso diretório do Ansible, execute:
```
ansible-playbook -i /etc/ansible/hosts playbook.yml
```
O *playbook.yml* irá conectar ao grupo de servidores [aws] e executar uma série de tarefas conforme pontuadas acima.

Aqui, podemos ver o fonte do nosso *playbook.yml*:
```
---
- hosts: aws
  become: true
  vars:
     packages: ["conntrack"]

  pre_tasks:
     - raw: apt-get -y install curl rsync sudo

  tasks:
    - name: Instalando pacotes
      apt: name={{ item }} state=latest
      with_items: "{{ packages }}"

    # Instalação Kubernetes
    - include: ./includes/playbook-instalacao-kubernetes.yml

    # Instalação do Docker
    - include: ./includes/playbook-instalacao-docker.yml

    # Instalação do Docker-composer
    - include: ./includes/playbook-instalacao-docker-composer.yml

    # Sincroniza configuracoes Kubernetes
    - include: ./includes/playbook-sincroniza-configuracoes-kubernetes.yml

    # Sincroniza configuracoes Docker-composer para app frontend
    - include: ./includes/playbook-sincroniza-configuracoes-docker-composer.yml

    # Cria servicos Kubernetes
    - include: ./includes/playbook-cria-servicos-kubernetes.yml

    # Inicia Docker com aplicação frontend
    - include: ./includes/playbook-inicia-docker-app-fronend.yml


```

Os demais arquivos declarados nos índices `- include:` do *playbook.yml* são parte integrante da implantação. Estes, podem ser acessados livremente seguindo cada qual seu diretório.

A resposta esperada após a execução do ansible-playbook, deve ser algo assim:

```
marcelo@28122019 /var/www/dev-ops-iac$ ansible-playbook -i /etc/ansible/hosts playbook.yml

PLAY [aws] *********************************************************************

TASK [setup] *******************************************************************
ok: [3.235.185.136]

TASK [raw] *********************************************************************
changed: [3.235.185.136]

TASK [Instalando pacotes] ******************************************************
ok: [3.235.185.136] => (item=[u'conntrack'])

TASK [Instalando Kubernetes kubectl] *******************************************
changed: [3.235.185.136]
 [WARNING]: Consider using get_url or uri module rather than running curl


TASK [Instalando Kubernetes minikube] ******************************************
changed: [3.235.185.136]

TASK [Instalando Docker] *******************************************************
changed: [3.235.185.136]

TASK [Adicionando usuario ubuntu ao grupo Docker] ******************************
changed: [3.235.185.136]

TASK [Instalando Docker-Composer] **********************************************
changed: [3.235.185.136]

TASK [Sincronizando configuracoes Kubernetes com a instancia] ******************
ok: [3.235.185.136]

TASK [Sincronizando configuracoes Docker-composer para app-frontend] ***********
ok: [3.235.185.136]

TASK [Deletando clusters existentes] *******************************************
changed: [3.235.185.136]

TASK [Criando um cluster kubernetes com minikube] ******************************
changed: [3.235.185.136]

TASK [Criando controlador do tipo Deployment aplicacao backend] ****************
changed: [3.235.185.136]

TASK [Criando servico LoadBance para acesso aos Pods backend] ******************
changed: [3.235.185.136]

TASK [Iniciando aplicação front-end com Docker] ********************************
changed: [3.235.185.136]

PLAY RECAP *********************************************************************
3.235.185.136              : ok=15   changed=11   unreachable=0    failed=0   

marcelo@28122019 /var/www/dev-ops-iac$ ansible-playbook -i /etc/ansible/hosts playbook.yml

PLAY [aws] *********************************************************************

TASK [setup] *******************************************************************
ok: [3.235.185.136]

TASK [raw] *********************************************************************
changed: [3.235.185.136]

TASK [Instalando pacotes] ******************************************************
ok: [3.235.185.136] => (item=[u'conntrack'])

TASK [Instalando Kubernetes kubectl] *******************************************
changed: [3.235.185.136]
 [WARNING]: Consider using get_url or uri module rather than running curl


TASK [Instalando Kubernetes minikube] ******************************************
changed: [3.235.185.136]

TASK [Instalando Docker] *******************************************************
changed: [3.235.185.136]

TASK [Adicionando usuario ubuntu ao grupo Docker] ******************************
changed: [3.235.185.136]

TASK [Instalando Docker-Composer] **********************************************
changed: [3.235.185.136]

TASK [Sincronizando configuracoes Kubernetes com a instancia] ******************
ok: [3.235.185.136]

TASK [Sincronizando configuracoes Docker-composer para app-frontend] ***********
ok: [3.235.185.136]

TASK [Deletando clusters existentes] *******************************************
changed: [3.235.185.136]

TASK [Criando um cluster kubernetes com minikube] ******************************
changed: [3.235.185.136]

TASK [Criando controlador do tipo Deployment aplicacao backend] ****************
changed: [3.235.185.136]

TASK [Criando servico LoadBance para acesso aos Pods backend] ******************
changed: [3.235.185.136]

TASK [Iniciando aplicação front-end com Docker] ********************************
changed: [3.235.185.136]

PLAY RECAP *********************************************************************
3.235.185.136              : ok=15   changed=11   unreachable=0    failed=0  
```

Na saída do comando acima, o servidor provisionado foi *3.235.185.136*. Se tudo ocorreu bem, basta agora acessar a aplicação front-end que chamará por sua vez, o Loadbalance das aplicações back-end.

```
http://3.235.185.136 # Este endereço é hipotético e portanto, não está disponível!
# Para testar efetivamente toda implementação, use o endereço IP gerado a partir do provisionamento da instância AWS EC2.
```

Obrigado!
Marcelo Viana

https://www.linkedin.com/m/in/marcelovianaalmeida
