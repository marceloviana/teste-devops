variable "region" {
    default = "us-east-1"
}
variable "shared_cred_file" {
    default = "~/.aws/credentials"
}
provider "aws" {
    region = "${var.region}"
    shared_credentials_file = "${var.shared_cred_file}"
    profile = "default"
}
resource "aws_instance" "ubuntu" {
    ami = "ami-00a208c7cdba991ea"
    instance_type = "t2.medium"
    key_name = "aws-ubuntu-ec2"
    tags {
        Name = "UbuntuTerraForm"
    }
}
